# Warranty Product

## Features

- Built with [Vite.js](https://vitejs.dev/guide/)
- Ant Design UI Library provided by [Ant Design](https://ant.design/)

---

## Installation

To install and run the React App:

```sh
yarn install
yarn start
```

The `yarn start` command will Run the app in the development mode.
Open [http://localhost:5176](http://localhost:5176) to view it in the browser.

## Development

###### Overview:

- The `master` branch contains all the production code
